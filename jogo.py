import os
import pygame
from random import randint

def gameOver():
    gameOverFont = pygame.font.SysFont('arial black', 70)
    gameover = gameOverFont.render("GAME OVER", True, (255, 0, 0))
    janela.blit(gameover, (50, 100))

def zerouJogo():
    zeroujogoFont = pygame.font.SysFont('arial black', 80)
    zeroujogo_1 = zeroujogoFont.render("PLATINOU", True, (0, 255, 0))
    zeroujogo_2 = zeroujogoFont.render("O JOGO", True, (0, 255, 0))
    janela.blit(zeroujogo_1, (50, 100))
    janela.blit(zeroujogo_2, (90, 200))

# CONSTANTES DO PROJETO
IMG_ICON = os.path.join("img", "icon", "icon.png")
IMG_BACKGROUND = os.path.join("img", "background", "background.png")
MUSIC_CREDITOS = os.path.join("music", "creditos.ogg")
MUSIC_GAME_OVER = os.path.join("music", "musica_game_over.ogg")
MUSIC_MUSICA_1 = os.path.join("music", "musica_1.mp3")
MUSIC_LEVEL_UP = os.path.join("music", "level_up.ogg")
IMG_CARRO_PRINCIPAL = os.path.join("img", "carros", "carro_principal.png")
IMG_CARRO_PRINCIPAL_COLISAO = os.path.join("img", "carros", "carro_principal_colisao.png")
IMG_CARRO_INIMIGO_1 = os.path.join("img", "carros", "carro_inimigo_1.png")
IMG_CARRO_INIMIGO_2 = os.path.join("img", "carros", "carro_inimigo_2.png")
IMG_ARBUSTO_1 = os.path.join("img", "arbustos", "arbusto_1.png")
IMG_ARBUSTO_2 = os.path.join("img", "arbustos", "arbusto_2.png")
IMG_ARBUSTO_3 = os.path.join("img", "arbustos", "arbusto_3.png")
IMG_ARBUSTO_4 = os.path.join("img", "arbustos", "arbusto_4.png")
IMG_ARBUSTO_5 = os.path.join("img", "arbustos", "arbusto_5.png")
IMG_ARBUSTO_6 = os.path.join("img", "arbustos", "arbusto_6.png")

# VALORES

# CARRO INIMIGO 1
x_inimigo_1_valor = randint(35, 290)
y_inimigo_1_valor = 0
# CARRO INIMIGO 1
x_inimigo_2_valor = randint(35, 290)
y_inimigo_2_valor = 0
# ARBUSTO 1
x_arbusto_1_valor = randint(350, 470)
y_arbusto_1_valor = -200
# ARBUSTO 2
x_arbusto_2_valor = randint(350, 470)
y_arbusto_2_valor = -200
# ARBUSTO 3
x_arbusto_3_valor = randint(350, 470)
y_arbusto_3_valor = -200
# ARBUSTO 4
x_arbusto_4_valor = randint(350, 470)
y_arbusto_4_valor = -200
# ARBUSTO 5
x_arbusto_5_valor = randint(350, 470)
y_arbusto_5_valor = -200
# ARBUSTO 6
x_arbusto_6_valor = randint(350, 470)
y_arbusto_6_valor = -200
# SCORE
x_score_valor = 350
y_score_valor = 0
# LEVEL
x_level_valor = 350
y_level_valor = 40
# VELOCIDADES
velocidade_principal_valor = 2
velocidade_inimigo_1_valor = 1
velocidade_inimigo_2_valor = 2
velocidade_arbusto_valor = 1

count_loop = 0
count_score = 0
count_level = 0
level_Atual = 1

colisao = False

pygame.init()

# CARRO PRINCIPAL
x_principal = 150
y_principal = 450

# CARRO INIMIGO 1
x_inimigo_1 = x_inimigo_1_valor
y_inimigo_1 = y_inimigo_1_valor
# CARRO INIMIGO 2
x_inimigo_2 = x_inimigo_2_valor
y_inimigo_2 = y_inimigo_2_valor

# ARBUSTO 1
x_arbusto_1 = x_arbusto_1_valor
y_arbusto_1 = y_arbusto_1_valor
# ARBUSTO 2
x_arbusto_2 = x_arbusto_2_valor
y_arbusto_2 = y_arbusto_2_valor
# ARBUSTO 3
x_arbusto_3 = x_arbusto_3_valor
y_arbusto_3 = y_arbusto_3_valor
# ARBUSTO 4
x_arbusto_4 = x_arbusto_4_valor
y_arbusto_4 = y_arbusto_4_valor
# ARBUSTO 5
x_arbusto_5 = x_arbusto_5_valor
y_arbusto_5 = y_arbusto_5_valor
# ARBUSTO 6
x_arbusto_6 = x_arbusto_6_valor
y_arbusto_6 = y_arbusto_6_valor

# SCORE
x_score = x_score_valor
y_score = y_score_valor

# LEVEL
x_level = x_level_valor
y_level = y_level_valor

velocidade_principal = velocidade_principal_valor
velocidade_inimigo_1 = velocidade_inimigo_1_valor
velocidade_inimigo_2 = velocidade_inimigo_2_valor
velocidade_arbusto = velocidade_arbusto_valor

# CARREGANDO IMAGENS
fundo = pygame.image.load(IMG_BACKGROUND)
carro = pygame.image.load(IMG_CARRO_PRINCIPAL)
policia_1 = pygame.image.load(IMG_CARRO_INIMIGO_1)
policia_2 = pygame.image.load(IMG_CARRO_INIMIGO_2)

arbusto_1 = pygame.image.load(IMG_ARBUSTO_1)
arbusto_2 = pygame.image.load(IMG_ARBUSTO_2)
arbusto_3 = pygame.image.load(IMG_ARBUSTO_3)
arbusto_4 = pygame.image.load(IMG_ARBUSTO_4)
arbusto_5 = pygame.image.load(IMG_ARBUSTO_5)
arbusto_6 = pygame.image.load(IMG_ARBUSTO_6)

# CONSTRUINDO TELA
janela = pygame.display.set_mode((550, 600))

pygame.display.set_caption('Projeto Carros 2019 UNIBH')
pygame.display.set_icon(pygame.image.load(IMG_ICON))

# TOCANDO MUSICA
pygame.mixer.music.load(MUSIC_MUSICA_1)
pygame.mixer.music.play(1000)

# EXIBINDO PLACAR NA TELA
font = pygame.font.SysFont('arial black', 30)

janela_aberta = True

while janela_aberta:
    # pygame.time.delay(10)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            janela_aberta = False

    # MOVIMENTACAO CARRO PRINCIPAL
    comandos = pygame.key.get_pressed()
    if not colisao:
        if comandos[pygame.K_UP]:
            y_principal -= velocidade_principal
            if y_principal <= 0:
                y_principal = 5
        if comandos[pygame.K_DOWN]:
            y_principal += velocidade_principal
            if y_principal >= 500:
                y_principal = 500
        if comandos[pygame.K_LEFT]:
            x_principal -= velocidade_principal
            if x_principal <= 5:
                x_principal = 15
        if comandos[pygame.K_RIGHT]:
            x_principal += velocidade_principal
            if x_principal >= 300:
                x_principal = 290

    if y_inimigo_1 >= 600:
        y_inimigo_1 = -100
        x_inimigo_1 = randint(10, 290)
        count_score = count_score + 1
        count_level = count_level + 1
        score = font.render("Score: " + str(count_score), True, (255, 255, 255), (0, 0, 0))
    if y_inimigo_2 >= 600:
        y_inimigo_2 = -100
        x_inimigo_2 = randint(10, 290)
        count_score = count_score + 1
        count_level = count_level + 1
        score = font.render("Score: " + str(count_score), True, (255, 255, 255), (0, 0, 0))

    if not colisao:
        # MOVIMENTANDO INIMIGO 1
        y_inimigo_1 += velocidade_inimigo_1
        # MOVIMENTANDO INIMIGO 2
        y_inimigo_2 += velocidade_inimigo_2

    if y_arbusto_1 >= 600:
        y_arbusto_1 = -50
        x_arbusto_1 = randint(350, 470)
    if y_arbusto_2 >= 600:
        y_arbusto_2 = -50
        x_arbusto_2 = randint(350, 470)
    if y_arbusto_3 >= 600:
        y_arbusto_3 = -50
        x_arbusto_3 = randint(350, 470)
    if y_arbusto_4 >= 600:
        y_arbusto_4 = -50
        x_arbusto_4 = randint(350, 470)
    if y_arbusto_5 >= 600:
        y_arbusto_5 = -50
        x_arbusto_5 = randint(350, 470)
    if y_arbusto_6 >= 600:
        y_arbusto_6 = -50
        x_arbusto_6 = randint(350, 470)
    # CARREGANDO BACKGROUND
    janela.blit(fundo, (0, 0))

    # CARREGANDO CARRO PRINCIPAL
    janela.blit(carro, (x_principal, y_principal))
    janela.blit(policia_1, (x_inimigo_1, y_inimigo_1))
    janela.blit(policia_2, (x_inimigo_2, y_inimigo_2))

    if not colisao:
        if count_loop >= 0:
            # MOVIMENTANDO ARBUSTO 1
            y_arbusto_1 += velocidade_arbusto

        if count_loop >= 100:
            # MOVIMENTANDO ARBUSTO 2
            y_arbusto_2 += velocidade_arbusto

        if count_loop >= 200:
            # MOVIMENTANDO ARBUSTO 3
            y_arbusto_3 += velocidade_arbusto

        if count_loop >= 300:
            # MOVIMENTANDO ARBUSTO 4
            y_arbusto_4 += velocidade_arbusto

        if count_loop >= 400:
            # MOVIMENTANDO ARBUSTO 5
            y_arbusto_5 += velocidade_arbusto

        if count_loop >= 500:
            # MOVIMENTANDO ARBUSTO 6
            y_arbusto_6 += velocidade_arbusto

    janela.blit(arbusto_1, (x_arbusto_1, y_arbusto_1))
    janela.blit(arbusto_2, (x_arbusto_2, y_arbusto_2))
    janela.blit(arbusto_3, (x_arbusto_3, y_arbusto_3))
    janela.blit(arbusto_4, (x_arbusto_4, y_arbusto_4))
    janela.blit(arbusto_5, (x_arbusto_5, y_arbusto_5))
    janela.blit(arbusto_6, (x_arbusto_6, y_arbusto_6))

    # AUMENTADO A DIFICULDADE DO JOGO
    if count_level >= 9:
        velocidade_principal = velocidade_principal + (velocidade_principal / 100 * 5)
        velocidade_inimigo_1 = velocidade_inimigo_1 + (velocidade_inimigo_1 / 100 * 10)
        velocidade_inimigo_2 = velocidade_inimigo_2 + (velocidade_inimigo_2 / 100 * 10)
        level_Atual = level_Atual + 1
        level_up = pygame.mixer.Sound(MUSIC_LEVEL_UP)
        level_up.play()
        count_level = 0

    # COLISÃO CARRO 1
    if y_principal - 110 <= y_inimigo_1 and y_principal + 110 >= y_inimigo_1:
        if x_principal + 50 >= x_inimigo_1 and x_principal - 50 <= x_inimigo_1:
            carro = pygame.image.load(IMG_CARRO_PRINCIPAL_COLISAO)
            pygame.mixer.music.stop()
            if not colisao:
                music_game_over = pygame.mixer.Sound(MUSIC_GAME_OVER)
                music_game_over.play()
            colisao = True
            gameOver()
    # COLISÃO CARRO 2
    if y_principal - 110 <= y_inimigo_2 and y_principal + 110 >= y_inimigo_2:
        if x_principal + 50 >= x_inimigo_2 and x_principal - 50 <= x_inimigo_2:
            carro = pygame.image.load(IMG_CARRO_PRINCIPAL_COLISAO)
            pygame.mixer.music.stop()
            if not colisao:
                music_game_over = pygame.mixer.Sound(MUSIC_GAME_OVER)
                music_game_over.play()
            colisao = True
            gameOver()

    # COLISAO CARRO 1 - COLISÃO CARRO 2
    if y_inimigo_2 - 110 <= y_inimigo_1 and y_inimigo_2 + 110 >= y_inimigo_1:
        if x_inimigo_2 + 50 >= x_inimigo_1 and x_inimigo_2 - 50 <= x_inimigo_1:

            colisaoCarroInimigo = (x_inimigo_1 - x_inimigo_2)

            if x_inimigo_2 >= 10 and x_inimigo_2 <= 20:
                x_inimigo_2 = x_inimigo_2 - colisaoCarroInimigo
            elif x_inimigo_2 >= 21 and x_inimigo_2 <= 240:
                x_inimigo_2 = (x_inimigo_2 + colisaoCarroInimigo) + 50
            elif x_inimigo_2 >= 241 and x_inimigo_2 <= 290:
                x_inimigo_2 = (x_inimigo_2 + colisaoCarroInimigo) - 51

            if x_inimigo_2 < 0:
                x_inimigo_2 = 10
            elif x_inimigo_2 >= 300:
                x_inimigo_2 = 290

    score = font.render(" Score: " + str(count_score) + '       ', True, (255, 255, 255), (0, 0, 0))
    pos_texto = score.get_rect()
    pos_texto.center = (65, 50)
    janela.blit(score, (x_score, y_score))

    level = font.render(" Level: " + str(level_Atual) + '        ', True, (255, 255, 255), (0, 0, 0))
    level_texto = level.get_rect()
    level_texto.center = (65, 50)
    janela.blit(level, (x_level, y_level))

    count_loop += 1

    if level_Atual >= 50:
        pygame.mixer.music.stop()
        if not colisao:
            music_creditos = pygame.mixer.Sound(MUSIC_CREDITOS)
            music_creditos.play()
        colisao = True
        zerouJogo()

    pygame.display.update()

pygame.quit()
